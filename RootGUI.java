import java.awt.*;
import java.awt.event.*;
/**
*
* RootGUI class
*
* a graphical user interface used to estimate square root of a number
*
* ----- compile -----
*
* javac RootGUI.java
*
* ----- run -----
*
* java RootGUI
*
*/
// An AWT GUI program inherits from the top-level container java.awt.Frame
public class RootGUI extends Frame implements KeyListener {
      // This class acts as KeyEvent Listener
 
    private TextField tfInput;  // Single-line TextField to receive tfInput key
    private TextField tfEstimate;  // Single-line TextField to receive tfEstimate key
    private TextField tfHistory;  // Single-line TextField to receive tfHistory key
    private TextArea taDisplay; // Multi-line TextArea to taDisplay result
    private int find;
    private int estimate;
    private int history;
    private String sfind;
    private String sestimate;
    private String shistory;
    private RootFind rootFind;
 
   // Constructor to setup the GUI components and event handlers
    public RootGUI() {
      setLayout(new FlowLayout()); // "super" frame sets to FlowLayout

      add(new Label("Enter number: "));
      tfInput = new TextField(10);
      add(tfInput);
      add(new Label("Enter estimate: "));
      tfEstimate = new TextField(10);
      add(tfEstimate);
      add(new Label("Enter history: "));
      tfHistory = new TextField(10);
      add(tfHistory);
      taDisplay = new TextArea(20, 40); // 40 rows, 40 columns
      add(taDisplay);

      tfInput.addKeyListener(this);
      tfEstimate.addKeyListener(this);
      tfHistory.addKeyListener(this);
      find=-1;
      estimate=-1;
      history=-1;
      sfind="";
      sestimate="";
      shistory="";
      rootFind=new RootFind();
         // tfInput TextField (source) fires KeyEvent.
         // tfInput adds "this" object as a KeyEvent listener.
 
      setTitle("KeyEvent Demo"); // "super" Frame sets title
      setSize(800, 400);         // "super" Frame sets initial size
      setVisible(true);          // "super" Frame shows
   }
 
   // The entry main() method
   public static void main(String[] args) {
      new RootGUI();  // Let the constructor do the job
   }
 
   /** KeyEvent handlers */
   // Called back when a key has been typed (pressed and released)
   @Override
   public void keyTyped(KeyEvent evt) {
        char typed=evt.getKeyChar();
        double record[];
        double found;
        if(27==(int)typed){ // esc key type, exit
            System.exit(0);
        }
        if(evt.getSource()==this.tfInput){ // number used to find square root of typed
            if(8==(int)typed){
                sfind="";
                this.tfInput.setText("");
                find=-1;
            }else{
                sfind=sfind+typed;
                find=Integer.parseInt(sfind);
                taDisplay.append("Number to find square root of "+find+"\n");
            }
        }
        else if(evt.getSource()==this.tfEstimate){ // initial estimate typed
            if(8==(int)typed){
                sestimate="";
                this.tfEstimate.setText("");
                estimate=-1;
            }else{
                sestimate=sestimate+typed;
                estimate=Integer.parseInt(sestimate);
                taDisplay.append("Initial estimate "+estimate+"\n");
            }
        }
        else if(evt.getSource()==this.tfHistory){ // history of computation typed
            if(8==(int)typed){
                shistory="";
                this.tfHistory.setText("");
                history=-1;
            }else{
                shistory=shistory+typed;
                history=Integer.parseInt(shistory);
            }
        }
        if((find>0)&&(estimate>0)&&(history>30)){ // everything typed, do computation
            record=new double[history];
            found=rootFind.iterate(Double.valueOf(estimate),Double.valueOf(find),record);
            taDisplay.append("Found estimate "+found+"\n"); // display result
        }
   }
 
   // Not Used, but need to provide an empty body for compilation
   @Override public void keyPressed(KeyEvent evt) { }
   @Override public void keyReleased(KeyEvent evt) { }
}
