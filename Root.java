/**
*
* Root class
*
* used to compute a square root of a number using iterative method (Newton Raphson)
*
* iteration given as xn=x(n-1)-f(x(n-1))/f'(x(n-1))
*
* ----- compile -----
*
* javac Root
*
* ----- run example -----
*
* java Root 7 3 30 (estimate square root of 7, initial estimate 3, history of computation 30 items)
*
* ----- example output -----
*
* 2.645833333333333
*
* ----- references -----
*
* http://www.sosmath.com/calculus/diff/der07/der07.html
*
*/
class Root{
    public static void main(String args[]){
        double find; // a value used to find a square root
        double estimate; // initial estimate
        int i,count; // number of estimates to record
        double[] record;
        RootFind rootFind;
        if(args.length !=3 ){ // need 3 command line arguments
            System.out.println("need find, estimate and count as initial arguments");
            return;
        }
        find=Double.parseDouble(args[0]);
        estimate=Double.parseDouble(args[1]);
        count=Integer.parseInt(args[2]);
        record=new double[count];
        rootFind=new RootFind();
        System.out.println(Double.toString(rootFind.iterate(estimate,find,record)));
        for(i=0;i<count;i++){
            System.out.println(record[i]);
        }
    }
}
