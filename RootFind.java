/**
*
* RootFind class
*
* used to compute a square root of a number using iterative method (Newton Raphson)
*
* iteration given as xn=x(n-1)-f(x(n-1))/f'(x(n-1))
*
*/
class RootFind{
    public double square(double estimate, double find){
        return (estimate*estimate - find); // a quadratic function x^2 - c
    }
    public double derivative(double estimate){
        return 2*estimate; // derivate of a quadratic function x^2 - c
    }
    public double iterate(double estimate, double find,double[] record){
        double next,prev;
        double change=0.001; // if change less, stop iterate
        int count=0;
        prev=estimate;
        for(;true;){
            next=prev-(square(prev,find)/derivative(prev));
            record[count++]=prev;
            record[count++]=next;
            record[count++]=square(prev,find);
            record[count++]=derivative(prev);
            record[count++]=square(prev,find)/derivative(prev);
            if(Math.abs(next-prev)<change){
                break;
            }
            prev=next;
        }
        return prev;
    }
}
