Discussion 7
------------

- Root.java (iterative method to find square root of a number Newton Raphson, check comments for details)
- RootGUI.java (iterative method to find square root, graphical user interface , check comments for details)
- RootFind.java (iterative method to find square root of a number Newton Raphson, a set of methods, check comment for details)

Test environment
----------------

- os lubuntu 16.04 kernel version 4.13.0
- javac version 11.0.5
